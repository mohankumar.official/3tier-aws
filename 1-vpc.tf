resource "aws_vpc" "my_vpc" {
      cidr_block       = "10.0.0.0/16"
        enable_dns_hostnames = true
          enable_dns_support   = true

            tags = {
                Name = "my-3-tier-app-vpc"
                  }
                  }

                  resource "aws_internet_gateway" "my_igw" {
                    vpc_id = aws_vpc.my_vpc.id

                      tags = {
                          Name = "my-igw"
                            }
                            }

                            resource "aws_subnet" "public_subnet_1" {
                              vpc_id            = aws_vpc.my_vpc.id
                                cidr_block        = "10.0.1.0/24"
                                  availability_zone = "your-az1"

                                    tags = {
                                        Name = "public-subnet-1"
                                          }
                                          }

                                          resource "aws_subnet" "public_subnet_2" {
                                            vpc_id            = aws_vpc.my_vpc.id
                                              cidr_block        = "10.0.2.0/24"
                                                availability_zone = "your-az2"

                                                  tags = {
                                                      Name = "public-subnet-2"
                                                        }
                                                        }

                                                        resource "aws_subnet" "private_subnet_1" {
                                                          vpc_id            = aws_vpc.my_vpc.id
                                                            cidr_block        = "10.0.3.0/24"
                                                              availability_zone = "your-az1"

                                                                tags = {
                                                                    Name = "private-subnet-1"
                                                                      }
                                                                      }

                                                                      resource "aws_subnet" "private_subnet_2" {
                                                                        vpc_id            = aws_vpc.my_vpc.id
                                                                          cidr_block        = "10.0.4.0/24"
                                                                            availability_zone = "your-az2"

                                                                              tags = {
                                                                                  Name = "private-subnet-2"
                                                                                    }
                                                                                    }

                                                                                    resource "aws_route_table" "public_route_table" {
                                                                                      vpc_id = aws_vpc.my_vpc.id

                                                                                        route {
                                                                                            cidr_block = "0.0.0.0/0"
                                                                                                gateway_id = aws_internet_gateway.my_igw.id
                                                                                                  }
                                                                                                  }

                                                                                                  resource "aws_route_table_association" "public_subnet_1_association" {
                                                                                                    subnet_id      = aws_subnet.public_subnet_1.id
                                                                                                      route_table_id = aws_route_table.public_route_table.id
                                                                                                      }

                                                                                                      resource "aws_route_table_association" "public_subnet_2_association" {
                                                                                                        subnet_id      = aws_subnet.public_subnet_2.id
                                                                                                          route_table_id = aws_route_table.public_route_table.id
                                                                                                          }

                                                                                                          resource "aws_eip" "nat_eip" {
                                                                                                            vpc = true
                                                                                                            }

                                                                                                            resource "aws_nat_gateway" "my_nat_gateway" {
                                                                                                              allocation_id = aws_eip.nat_eip.id
                                                                                                                subnet_id     = aws_subnet.public_subnet_1.id
                                                                                                                }

                                                                                                                resource "aws_route_table" "private_route_table" {
                                                                                                                  vpc_id = aws_vpc.my_vpc.id

                                                                                                                    route {
                                                                                                                        cidr_block     = "0.0.0.0/0"
                                                                                                                            nat_gateway_id = aws_nat_gateway.my_nat_gateway.id
                                                                                                                              }
                                                                                                                              }

                                                                                                                              resource "aws_route_table_association" "private_subnet_1_association" {
                                                                                                                                subnet_id      = aws_subnet.private_subnet_1.id
                                                                                                                                  route_table_id = aws_route_table.private_route_table.id
                                                                                                                                  }

                                                                                                                                  resource "aws_route_table_association" "private_subnet_2_association" {
                                                                                                                                    subnet_id      = aws_subnet.private_subnet_2.id
                                                                                                                                      route
                                                                                                                                      
}
