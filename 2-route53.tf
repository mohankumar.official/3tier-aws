provider "aws" {
      region = "eu-west-2"     # replace with your preferred AWS region
}

resource "aws_route53_zone" "legitvector_zone" {
      name = "legitvector.shop"      #your domain name

        vpc {
                vpc_id = "vpc-0256ba67a5dd3e368"      # replace with your VPC id
        }

          tags = {
                  Environment = "test"
                      Name        = "my-zone"
          }
}

output "name_servers" {
      description = "The name servers for our zone"
        value       = aws_route53_zone.my_zone.name_servers
}

}
          }
        }
}
}
